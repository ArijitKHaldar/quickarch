#!/bin/bash

set -a
script_run_dir="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
functions_dir=$script_run_dir/scripts
user_info_file=$script_run_dir/user_info.txt
pacman_packages=$script_run_dir/packagelist.txt
aur_packages=$script_run_dir/aur_packagelist.txt
disk_partitions=$script_run_dir/partitions.txt
set +a

# Generate user file if not already present
if [[ ! -f $user_info_file ]]; then
    touch -f $user_info_file
fi
if [[ ! -f $disk_partitions ]]; then
    touch -f $disk_partitions
fi
# Delete any configurations present if old config file is there
if grep -qEq "^.*" $user_info_file; then
    sed -i -e "/^.*/d" $user_info_file
fi
if grep -qEq "^.*" $disk_partitions; then
    sed -i -e "/^.*/d" $disk_partitions
fi

setfont ter-v22b
clear
# Synchronise system clock with Network Time Protocol for current booted system
timedatectl set-ntp true
echo -ne "----------------------------------------------------------
        Welcome to Arch Linux Installation Wizard
    Please wait and go through all the configurations
till the system prompts for unattended installation stage
----------------------------------------------------------
 The current time and date is:\n"
timedatectl status
echo -ne "\n\nIf date and time is wrong, please press Ctrl+C to stop this script"
sleep 10

# CHECK BIOS or UEFI and decide which mode to use for installation
function detectbootmode {
    if [[ -d /sys/firmware/efi/efivars ]]; then
        echo "TRUE"
    else
        echo "FALSE"
    fi
}
function bootmode {
    uefi=$(detectbootmode)
    if [[ $uefi == "TRUE" ]]; then
        echo -ne "\n\n\nThe system detected that UEFI is available\n"
        echo -ne "Please choose the installation type
1. UEFI with GPT
2. BIOS with GPT
3. Both\n"
        read -p "Enter the appropriate option number [default is 1]: " boottype
        case $boottype in
            2)
                echo "Boot_mode=bios_gpt" >> $user_info_file;;
            3)
                echo "Boot_mode=uefi_bios_gpt" >> $user_info_file;;
            *)
                echo "Boot_mode=uefi_gpt" >> $user_info_file;;
        esac
    else
        echo -ne "\n\n\nThe system detected that BIOS is available\n"
        echo -ne "Please choose the installation type
1. BIOS with MBR
2. BIOS with GPT\n"
        read -p "Enter the appropriate option number [default is 1]: " boottype
        case $boottype in
            2)
                echo "Boot_mode=bios_gpt" >> $user_info_file;;
            *)
                echo "Boot_mode=bios_mbr" >> $user_info_file;;
        esac
    fi
}

nc=$(grep -c ^processor /proc/cpuinfo)
total_mem=$(cat /proc/meminfo | grep -i 'memtotal' | grep -o '[[:digit:]]*')

if [[  $total_mem -gt 7880000 ]]; then
    #sed -i "s/#BUILDDIR/BUILDDIR/g" /etc/makepkg.conf
    sed -i "s/#MAKEFLAGS=\"-j2\"/MAKEFLAGS=\"-j$(nproc)\"/g" /etc/makepkg.conf
    sed -i "s/COMPRESSXZ=(xz -c -z -)/COMPRESSXZ=(xz -c -z --threads=$nc -)/g" /etc/makepkg.conf
    sed -i "s/COMPRESSZST=(zstd -c -z -q -)/COMPRESSXZ=(xz -c -z -q --threads=$nc -)/g" /etc/makepkg.conf
fi

proc_type=$(lscpu)
if grep -qE "GenuineIntel" <<< ${proc_type}; then
    echo "Processor_microcode=intel-ucode" >> $user_info_file
elif grep -qE "AuthenticAMD" <<< ${proc_type}; then
    echo "Processor_microcode=amd-ucode" >> $user_info_file
fi

gpu_type=$(lspci)
if grep -qE "NVIDIA|GeForce" <<< ${gpu_type}; then
    echo "Graphics_drivers=nvidia nvidia-xconfig" >> $user_info_file
elif lspci | grep -qE "Radeon|AMD"; then
    echo "Graphics_drivers=xf86-video-amdgpu" >> $user_info_file
elif grep -qE "Integrated Graphics Controller|Intel Corporation UHD|VGA compatible controller: Intel" <<< ${gpu_type}; then
    echo "Graphics_drivers=libva-intel-driver libvdpau-va-gl vulkan-intel" >> $user_info_file
fi

# Partitioning Disks
function drivessd {
    read -p "Is this an SSD? [Y/n]: " ssdcheck
    case $ssdcheck in
        n|N|NO|no|No|nO)
            echo "FALSE";;
        *)
            echo "TRUE";;
    esac
}
function drivepartition {
    echo -ne "\n\n\nSelect the drive where you want to install\n"
    echo "$(lsblk -n --output TYPE,KNAME,SIZE | awk '$1=="disk"{print "/dev/"$2"\t-\t"$3}')"
    echo -ne "Please type full path [eg. /dev/sda] then press enter\n"
    read -p "Installation drive: " drive
    drivetype=$(drivessd)
    echo "Drive=$drive" >> $user_info_file
    if [[ $drivetype == "TRUE" ]]; then
        echo "Drive_type=ssd" >> $user_info_file
    else
        echo "Drive_type=hdd" >> $user_info_file
    fi

    echo "You chose $drive. This will be formatted and you will LOSE ALL EXISTING DATA"
    echo -ne "Please confirm that you understand by typing YES to the question below\n\n"
    read -p "$drive is the drive which you want to format and install the new system on?: " tmp
    case $tmp in
        YES|Y|Yes|yes|y);;
        *) exit 1;;
    esac

    echo -e "\n\n\n"
    read -p "What is the ROOT partition size you want? [eg. 120GiB]: " rootsize
    echo "Root_size=$rootsize" >> $user_info_file
    read -p "ROOT Partition Label (under 16 characters all caps no special characters): " root_label
    echo "Root_part_label=$root_label" >> $user_info_file
    if [[ $uefi == "FALSE" ]]; then
        echo -ne "With BIOS/MBR boot method, maximum 4 partitions can be made\n"
        read -p "Do you want a SWAP partition? [Y/n]: " ans
        case $ans in
            n|N|NO|no|No|nO)
                echo "Swap_part=FALSE" >> $user_info_file;;
            *)
                echo "Swap_part=TRUE" >> $user_info_file
                read -p "What is the SWAP size you want? [eg. 8GiB]: " swapsize
                echo "Swap_size=$swapsize" >> $user_info_file
                read -p "SWAP Partition Label (under 16 characters all caps no special characters): " swap_label
                echo "Swap_part_label=$swap_label" >> $user_info_file;;
        esac
        read -p "Do you want a HOME partition? [Y/n]: " ans
        case $ans in
            n|N|NO|no|No|nO)
                echo "Home_part=FALSE" >> $user_info_file;;
            *)
                echo "Home_part=TRUE" >> $user_info_file
                echo $'The HOME partition can be made to take up the remaining space in drive \nafter all other partitions are made by pressing ENTER without any value for the next question'
                read -p "What is the HOME partition size you want? [eg. 100GiB]: " homesize
                echo "Home_size=$homesize" >> $user_info_file
                read -p "HOME Partition Label (under 16 characters all caps no special characters): " home_label
                echo "Home_part_label=$home_label" >> $user_info_file;;
        esac
    else
        if grep -iq "uefi_gpt" "$user_info_file"; then
            read -p "What is the EFI System partition size you want? [eg. 512MiB]: " efisize
            echo "EFI_part_size=$efisize" >> $user_info_file
            read -p "EFI Partition Label (under 16 characters all caps no special characters): " efi_label
            echo "EFI_part_label=$efi_label" >> $user_info_file
        fi
        read -p "Do you want a SWAP partition? [Y/n]: " ans
        case $ans in
            n|N|NO|no|No|nO)
                echo "Swap_part=FALSE" >> $user_info_file;;
            *)
                echo "Swap_part=TRUE" >> $user_info_file
                read -p "What is the SWAP size you want? [eg. 8GiB]: " swapsize
                echo "Swap_size=$swapsize" >> $user_info_file
                read -p "SWAP Partition Label (under 16 characters all caps no special characters): " swap_label
                echo "Swap_part_label=$swap_label" >> $user_info_file;;
        esac
        read -p "Do you want a HOME partition? [Y/n]: " ans
        case $ans in
            n|N|NO|no|No|nO)
                echo "Home_part=FALSE" >> $user_info_file;;
            *)
                echo "Home_part=TRUE" >> $user_info_file
                echo $'The HOME partition can be made to take up the remaining space in drive \nafter all other partitions are made by pressing ENTER without any value for the next question'
                read -p "What is the HOME partition size you want? [eg. 100GiB]: " homesize
                echo "Home_size=$homesize" >> $user_info_file
                read -p "HOME Partition Label (under 16 characters all caps no special characters): " home_label
                echo "Home_part_label=$home_label" >> $user_info_file;;
        esac
    fi
}

# Now prepare the partitions
function driveformat {
    echo -ne "\n\n\n"
    if grep -iq "uefi_gpt" "$user_info_file"; then
        read -p "Give a name to your EFI System Partition [eg. UEFI]" efi_name
        echo "EFI_part_name=$efi_name" >> $user_info_file
    fi
    echo -ne "What is the partition format you would like to use for your root (and home partition if present)
1. EXT4
2. BTRFS\n\n"
    read -p "Please enter option number corresponding to your choice [default is 1]: " tmp
    case $tmp in
        2) echo "Format_type=btrfs" >> $user_info_file
            if [[ $drivetype == "TRUE" ]]; then
                read -p "Does your SSD support async queued TRIM (SATA 3.1)? [y/N]: " tmp1
                case $tmp1 in
                    y|Y|yes|YES|Yes)
                        mount_opn='noatime,commit=120,compress=zstd,ssd,space_cache=v2,discard=sync';;
                    *)
                        mount_opn='noatime,commit=120,compress=zstd,ssd,space_cache=v2,discard=async';;
                esac
            else
                mount_opn='noatime,commit=120,compress=zstd,autodefrag'
            fi
            echo "Format_type=btrfs" >> $user_info_file
            echo "Btrfs_mount_options=$mount_opn" >> $user_info_file;;
        *) echo "Format_type=ext4" >> $user_info_file;;
    esac
    read -p "Give your ROOT partition a name [eg. ARCH]: " root_name
    echo "Root_part_name=$root_name" >> $user_info_file
    if grep -iq "home" "$user_info_file"; then 
        read -p "Give your HOME partition a name [eg. HOME]: " home_name
        echo "Home_part_name=$home_name" >> $user_info_file
    fi
    if grep -iq "swap" "$user_info_file"; then
        read -p "Give a name to your SWAP Partition [eg. SWAP]" swap_name
        echo "SWAP_part_name=$swap_name" >> $user_info_file
    fi
}


function user_choices {
    echo -e "\n\n\n"
    
    time_zone="$(curl --fail https://ipapi.co/timezone)"
    echo -ne "\nIs the detected timezone $time_zone correct?: "
    read tmp
    case $tmp in
        n|N|NO|no|No)
            echo -ne "\nA list of timezones will be displayed.\nPlease remember the correct option from the list and press q on the keyboard to close the list.
Use arrow keys up and down to scroll up or down the list."
            sleep 3
            timedatectl list-timezones
            read -p "Please type your timezone[eg. Canada/Eastern]: " time_zone;;
        *);;
    esac
    echo "Timezone=$time_zone" >> $user_info_file

    echo ""
    read -p "Do you need Indian Locale? [y/N]: " tmp
    case $tmp in
        y|Y|YES|yes|Yes)
            echo "Other_locale=IN" >> $user_info_file;;
        *);;
    esac
    echo ""
    read -p "If you needed to set a keyboard console font at the beginning, please type the name of the KEYMAP or press ENTER" keymap
    if [[ ! -z $keymap ]]; then
        echo "Keymap=$keymap" >> $user_info_file
    fi
    
    echo ""
    echo -ne "Your machine is identified on local network by its hostname\n"
    read -p "Please enter your device hostname [eg. ArchDESKTOP]: " hostnameset
    echo "Hostname=$hostnameset" >> $user_info_file

    while true
    do
        read -s -p "Enter root user password" paswd1
        echo ""
        read -s -p "Please re-enter the password" paswd2
        if $paswd1 == $paswd2; then
            echo "Root_password=$paswd2" >> $user_info_file
            break
        else
            echo -ne "\nSorry. Passwords don't match.\nTry Again\n"
        fi
    done
    while true
    do
        read -p "Please enter the primary user name (all small): " usrname
		    if [[ "${usrname,,}" =~ ^[a-z_]([a-z0-9_-]{0,31}|[a-z0-9_-]{0,30}\$)$ ]]; then
            echo "Sudo_user=${usrname,,}" >> $user_info_file
            break
        fi
        echo -ne "Incorrect username...\n"
    done
    while true
    do
        read -s -p "Enter $usrname user password" paswd1
        echo ""
        read -s -p "Please re-enter the password" paswd2
        if $paswd1 == $paswd2; then
            echo "Sudo_password=$paswd2" >> $user_info_file
            break
        else
            echo -ne "\nSorry. Passwords don't match.\nTry Again\n"
        fi
    done
}

function bootloader {
    echo -ne "\n\n\n"
    echo -ne "Which bootloader do you want to install?
1. GRUB
2. SystemD Boot
3. GRUB with EFI partition mounted on specified location\n"
efi_mount_point='/boot'
read -p "Please enter your choice number: " tmp
case $tmp in
    1)
        echo "Bootloader=grub" >> $user_info_file;;
    2)
        echo "Bootloader=systemd_boot" >> $user_info_file;;
    3)
        echo "Bootloader=grub" >> $user_info_file
        if grep -iq "efi" "$user_info_file"; then
            echo ""
            read -p "Where do you want the EFI partition to be mounted?[Default is /boot]: " efi_mount_point
        fi;;
    *)  echo -ne "Wrong Input\n"
        bootloader;;
esac
if grep -iq "efi" "$user_info_file"; then
    echo "EFI_location=$efi_mount_point" >> $user_info_file
fi
tmp=$(lscpu | awk '/Architecture:/{print $2}')
echo "Architecture=$tmp" >> $user_info_file
}

function kernel {
    echo -ne "\n\n\n"
    echo -ne "Which bootloader do you want to install?
1. Linux
2. Linux-LTS\n"
read -p "Please enter your choice number: " tmp
case $tmp in
    1)  echo "Kernel=linux" >> $user_info_file;;
    2)  echo "Kernel=linux-lts" >> $user_info_file;;
    *)  echo -ne "Wrong Input\n"
        kernel;;
esac

echo -ne "\n\n\n\nThe installation will begin shortly...\nPress Ctrl+C to stop right now\n"
sleep 5
clear

bootmode
drivepartition
driveformat
user_choices
bootloader
kernel

$functions_dir/diskpartition.sh
$functions_dir/diskformat.sh
$functions_dir/fstable.sh
$functions_dir/basepkginstall.sh
$functions_dir/changeroot.sh
$functions_dir/timesynchron.sh
$functions_dir/pkginstall.sh
$functions_dir/initramfsgen.sh
$functions_dir/localhostconf.sh
$functions_dir/localization.sh
$functions_dir/userpass.sh
$functions_dir/bootloader.sh

echo -ne "\n\n\nCompleted installation successfully. Exiting installer."
exit
umount -la /mnt
echo -ne "\nPlease reboot now"
