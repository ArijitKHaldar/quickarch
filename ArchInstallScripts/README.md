## Arch Linux Installation Script
![Arch Linux Logo](./Screenshots/ArchLogo.png "Logo of Arch Linux")

Before going into the "how-to's", let's get a few things straight first.


- ##### Why use [Arch Linux](https://www.archlinux.org "Arch Official Website")
Arch Linux is a rolling-release GNU/Linux distribution that is not based on any other parent distributions. It provides a very high degree or customizability and minimality, while also not making the installation and maintenance of the distro as demanding as [Gentoo](https://www.gentoo.org/).
    
The advantage of a rolling-release distro is that there is no version number. Once properly installed and maintained, you will never have to even think about a big version update. All updates are made in-place by just updating each software components installed, thus, your distro always stays the latest and greatest. To top it all, Arch gets the newest updates of nearly every software earlier than many mainstream distros.

> Things that you should keep in mind is that the newest softwares may occasionally have some bugs. It is your responsibility to revert back any software updates which might have caused an issue or keep an eye on the [Arch Linux website](https://archlinux.org/news/) to be acquainted with any new updates that might need manual intervention with the configurations to have your Arch Linux journey smooth

Enjoy the learning experience :smile:

- ##### Why not use `archinstall`
Anyone who is familiar with the new Arch Linux installation iso, will know that Arch Linux has its own installation helper called [`archinstall`](https://wiki.archlinux.org/title/Archinstall). 


Let's get this very clear. **This script, in no way, was intended to improve or replace the official Arch Linux installation script**. While Arch Linux is and always will be design so that the user installs and configures his/her system on their own through the command-line; but the Arch Linux team has done a fabulous job of making an installation helper script which is much more comprehensive and user-friendly to work with.

This script is intended for the users who knows what they want, just needs a quick and easy way to setup and run the installation. It focuses on the Arch Wiki installation guide and gives a base script, which users can work on to make their own installation script, which they can use to whip-up their own Arch Linux installation, whenever they need to, coz, let's be honest, hardware do fail. Even the best of us sometimes can't get the time to do the complete install all over again, but we, as tinkerers, won't be happy with a generic installation; since we are used to the customized installation that is so much a lifestyle now for us, rather than just an operating system.

Isn't it cool to have all your configurations scripted, so, when things go down, you just run it, have a cup of coffee, and rest assured that your system will return to the exact state you scripted your system to do? That's the only aim of this installation script.

### Installation Procedure
<details>
<summary> Create Installation Media </summary>
The installation media is the working Arch Linux installation which provides a shell to install Arch on the target storage device.
The storage device where the installation media boots from MUST be physically separate from the storage device where you want to install Arch.


The installation media can be
1. An USB flash drive of atleast 8GiB where you put the [Arch Linux bootable iso](https://archlinux.org/download/ "Arch ISO Download website").
2. Arch Linux [portable installation](https://youtu.be/9F_LFTUn75Q "YouTube video demonstrating Arch Portable installation") on a removable drive.
3. A second storage media in your PC/laptop which has Arch Linux running already.

For `1`, use [Rufus](https://rufus.ie/en/) or [Ventoy](https://www.ventoy.net/en/download.html) to make your pen drive bootable with the iso you downloaded. Please be aware that the pen drive will be formatted and you will lose any data in the pen drive.

Since `2` and `3` already has the installation in it, for any of the 3 options, the next step is to shutdown your PC/laptop completely, insert the installation medium, go into the BIOS/UEFI settings by pressing the F2 or F8 or DEL key when the machine is booting up multiple times (turn off and try again if you miss the moment the BIOS boots up) and turn off SECURE BOOT, enable UEFI if available and set correct date & time, if not set. Go to boot order, and make the installation medium your first boot choice. Save and exit.

(Prior MS WINDOWS users who are dual booting needs to turn off Fast Startup and Hibernation and restart once before proceeding)
</details>
<details>
<summary> Boot Live Environment </summary>
If the boot order is detected correctly, Arch Linux should boot up, else choose the bootable media first by pressing the F12 or the boot selection key according to your motherboard defaults.
</details>

###### Set Console Keyboard
> The default keyboard used in Arch Linux iso is US Keyboard. If you have an US QUERTY keyboard and can type all letters, numbers and symbols correctly on the booted live environment, you can skip this step.
(P.S.: All Indian keyboards are US keyboards)

- List available Keyboard layouts
```bash
localectl list-keymaps
```
or
```bash
ls -l /usr/share/kbd/keymaps/**/*.map.gz | less 
```
Let's say that your keyboard is a Swiss/German keyboard, then your KEYMAP name is `de_CH-latin1`
```bash
loadkeys de_CH-latin1
```
[Remember that this sets only the keymap for the current environment and is not permanent. Please remember if you had to set this, since we have to set it permanently later]

###### Setting up Internet
If you are connected via an ethernet cable coming from the LAN port of your router, your internet might already have established correctly. Run the command below
```bash
ip -c a
```
![IP](./Screenshots/IP.png "Screenshot of ip -c a")

As you can see here, the network adapters recognized in my machine are **lo**, **enp1s0f1** & **wlp2s0**

**lo**: This is localhost, and we can ignore this.

**enp1s0f1**: This is my ethernet adapter. Any network adapter starting with the letter e will be the ethernet adapter.

**wlp2s0**: This is my WiFi adapter. Any network adapter starting with the letter w is the WiFi adapter.

Now, my ethernet cable was not plugged in, which you can see a `DOWN` written in red font for my ethernet adapter.
Similarly, my WiFi adapter has a green `UP`, denoting that my WiFi adapter was up and running.
The `inet` field gives you the IPV4 address assigned to your adapter, which you can see here that my WiFi adapter has an IP address starting with 192.168.. , which is provided by DHCP configured on my router.


The requirement here is to have atleast one of your network adapters, apart from your lo, to have an inet IP address assigned, with an UP mentioned.
If you are not connected via ethernet, WiFi has to be shown as UP (if not, use `rfkill` to unblock it), then connect using `wpa_supplicant` or `iwctl` utility.
<details>
<summary> iwctl</summary>
Type `iwctl` and press enter. The prompt will change to [iwd]#

```
station wlp2s0 connect WiFiSSID
```
Type the command above, with the name of your own WiFi adapter name and WiFi SSID to connect to and press enter. Enter your password on the prompt which follows. Your WiFi network will get connected.

</details>

<details>
<summary> wpa_supplicant</summary>

</details>

3G/4G dongles will need `mmcli` command to connect

###### Starting script

```bash
pacman -Sy
pacman -S git vim
git clone https://gitlab.com/arijitkhaldar/quickarch.git InstallterScripts
```
```bash
./InstallterScripts/ArchInstallScripts/install.sh
```

Before running the last line, it is suggested to view and edit packagelist.txt and partitions.txt inside the InstallterScripts folder.
