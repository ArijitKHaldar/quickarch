#!/usr/bin/env bash

drive_name=$(sed -n 's/.*Drive=\(.*\).*/\1/p' "$user_info_file")

if grep -iq "gpt" "$user_info_file"; then
    drive_layout=gpt
    if grep -iq "efi" "$user_info_file"; then
        efi_size=$(sed -n 's/.*EFI_part_size=\(.*\).*/\1/p' "$user_info_file")
        efi_label=$(sed -n 's/.*EFI_part_label=\(.*\).*/\1/p' "$user_info_file")
    fi    
    if grep -iq "bios_gpt" "$user_info_file"; then
        bios_size=1MiB
        bios_label="BIOSBOOT"
    fi
else
    drive_layout=dos
fi

root_size=$(sed -n 's/.*Root_size=\(.*\).*/\1/p' "$user_info_file")
root_label=$(sed -n 's/.*Root_part_label=\(.*\).*/\1/p' "$user_info_file")

tmp=$(sed -n 's/.*Home_part=\(.*\).*/\1/p' "$user_info_file")
if [[ $tmp == "TRUE" ]]; then
    home_size=$(sed -n 's/.*Home_size=\(.*\).*/\1/p' "$user_info_file")
    home_label=$(sed -n 's/.*Home_part_label=\(.*\).*/\1/p' "$user_info_file")
fi

tmp=$(sed -n 's/.*Swap_part=\(.*\).*/\1/p' "$user_info_file")
if [[ $tmp == "TRUE" ]]; then
    swap_size=$(sed -n 's/.*Swap_size=\(.*\).*/\1/p' "$user_info_file")
    swap_label=$(sed -n 's/.*Swap_part_label=\(.*\).*/\1/p' "$user_info_file")
fi

echo "label: $drive_layout" >> $disk_partitions
echo "unit: sectors" >> $disk_partitions
echo "device: $drive_name" >> $disk_partitions

if grep -iq "efi" "$user_info_file"; then
    echo "size=$efi_size, name=\"$efi_label\", type=\"uefi\"" >> $disk_partitions
fi
if grep -iq "bios_gpt" "$user_info_file"; then
    echo "size=$bios_size, name=\"$bios_label\", type=\"21686148-6449-6E6F-744E-656564454649\"" >> $disk_partitions
fi
if grep -iq "swap" "$user_info_file"; then
    echo "size=$swap_size, name=\"$swap_label\", type=\"swap\"" >> $disk_partitions
fi
if grep -iq "Home_part" "$user_info_file"; then
    if grep -iq "bios_mbr" "$user_info_file"; then
        echo "size=$root_size, name=\"$root_label\", type=\"linux\", bootable" >> $disk_partitions
    else
        echo "size=$root_size, name=\"$root_label\", type=\"linux\"" >> $disk_partitions
    fi    
    if [[ -z $home_size ]]; then
        echo "name=\"$home_label\", type=\"home\"" >> $disk_partitions
    else
        echo "size=$home_size, name=\"$home_label\", type=\"home\"" >> $disk_partitions
    fi
else
    if grep -iq "bios_mbr" "$user_info_file"; then
        echo "size=$root_size, name=\"$root_label\", type=\"linux\", bootable" >> $disk_partitions
    else
        echo "name=\"$root_label\", type=\"linux\"" >> $disk_partitions
    fi    
fi

sfdisk $drive_name < $disk_partitions
sfdisk --dump $drive_name > $disk_partitions
cp $disk_partitions ${disk_partitions}.bak
sed -i '1,/^[[:blank:]]*$/d' $disk_partitions
