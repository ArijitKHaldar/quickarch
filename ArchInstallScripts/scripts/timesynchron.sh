#!/usr/bin/env bash

time_zone=$(sed -n 's/.*Timezone=\(.*\).*/\1/p' "$user_info_file")

timedatectl set-ntp true
ln -sf /usr/share/zoneinfo/${time_zone} /etc/localtime
timedatectl set-timezone $time_zone

hwclock --systohc
