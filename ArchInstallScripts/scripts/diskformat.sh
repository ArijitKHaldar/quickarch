#!/usr/bin/env bash

if grep -iq "efi" "$user_info_file"; then
    efi_name=$(sed -n 's/.*EFI_part_name=\(.*\).*/\1/p' "$user_info_file")
    efi_mount_point=$(sed -n 's/.*EFI_location=\(.*\).*/\1/p' "$user_info_file")
fi
if grep -iq "swap" "$user_info_file"; then
    swap_name=$(sed -n 's/.*SWAP_part_name=\(.*\).*/\1/p' "$user_info_file")
fi
root_name=$(sed -n 's/.*Root_part_name=\(.*\).*/\1/p' "$user_info_file")
if grep -iq "Home_part" "$user_info_file"; then
    home_name=$(sed -n 's/.*Home_part_name=\(.*\).*/\1/p' "$user_info_file")
fi
if grep -iq "btrfs" "$user_info_file"; then
    mount_opn=$(sed -n 's/.*Btrfs_mount_options=\(.*\).*/\1/p' "$user_info_file")
fi

num_part=$(cut -d ' ' -f 1 $disk_partitions | wc -l)
partition=()
i=1
while [[ $i -le $num_part ]]; do
    partition[i]=$(cut -d ' ' -f 1 $disk_partitions | head -$i | tail +$i)
    ((i++))
done

if grep -iq "swap" "$user_info_file"; then
    mkswap -L $swap_name ${partition[2]}
    swapon ${partition[2]}
    if grep -iq "ext4" "$user_info_file"; then
        mkfs.ext4 -L $root_name ${partition[3]}
        mount ${partition[3]} /mnt
        if grep -iq "Home_part" "$user_info_file"; then
            mkfs.ext4 -L $home_name ${partition[4]}
            mount --mkdir ${partition[4]} /mnt/home
        fi
    elif grep -iq "btrfs" "$user_info_file"; then
        mkfs.btrfs -L $root_name ${partition[3]}
        mount ${partition[3]} /mnt
        btrfs subvolume create /mnt/@
        btrfs subvolume create /mnt/@var
        btrfs subvolume create /mnt/@opt
        umount /mnt
        if grep -iq "Home_part" "$user_info_file"; then
            mkfs.btrfs -L $home_name ${partition[4]} 
            mount ${partition[4]} /mnt
            btrfs subvolume create /mnt/@home
            umount /mnt
            mount -o ${mount_opn},subvol=@ ${partition[3]} /mnt
            mount --mkdir -o ${mount_opn},subvol=@opt ${partition[3]} /mnt/opt
            mount --mkdir -o ${mount_opn},subvol=@var ${partition[3]} /mnt/var
            mount --mkdir -o ${mount_opn},subvol=@home ${partition[4]} /mnt/home
        else
            mount ${partition[3]} /mnt
            btrfs subvolume create /mnt/@home
            umount /mnt
            mount -o ${mount_opn},subvol=@ ${partition[3]} /mnt
            mount --mkdir -o ${mount_opn},subvol=@opt ${partition[3]} /mnt/opt
            mount --mkdir -o ${mount_opn},subvol=@var ${partition[3]} /mnt/var
            mount --mkdir -o ${mount_opn},subvol=@home ${partition[3]} /mnt/home
        fi
    fi
else
    if grep -iq "ext4" "$user_info_file"; then
        mkfs.ext4 -L $root_name ${partition[2]}
        mount ${partition[2]} /mnt
        if grep -iq "Home_part" "$user_info_file"; then
            mkfs.ext4 -L $home_name ${partition[3]}
            mount --mkdir ${partition[3]} /mnt/home
        fi
    elif grep -iq "btrfs" "$user_info_file"; then
        mkfs.btrfs -L $root_name ${partition[2]}
        mount ${partition[2]} /mnt
        btrfs subvolume create /mnt/@
        btrfs subvolume create /mnt/@var
        btrfs subvolume create /mnt/@opt
        umount /mnt
        if grep -iq "Home_part" "$user_info_file"; then
            mkfs.btrfs -L $home_name ${partition[3]}
            mount ${partition[3]} /mnt
            btrfs subvolume create /mnt/@home
            umount /mnt
            mount -o ${mount_opn},subvol=@ ${partition[2]} /mnt
            mount --mkdir -o ${mount_opn},subvol=@opt ${partition[2]} /mnt/opt
            mount --mkdir -o ${mount_opn},subvol=@var ${partition[2]} /mnt/var
            mount --mkdir -o ${mount_opn},subvol=@home ${partition[3]} /mnt/home
        else
            mount ${partition[2]} /mnt
            btrfs subvolume create /mnt/@home
            umount /mnt
            mount -o ${mount_opn},subvol=@ ${partition[2]} /mnt
            mount --mkdir -o ${mount_opn},subvol=@opt ${partition[2]} /mnt/opt
            mount --mkdir -o ${mount_opn},subvol=@var ${partition[2]} /mnt/var
            mount --mkdir -o ${mount_opn},subvol=@home ${partition[2]} /mnt/home
        fi
    fi
fi
if grep -iq "efi" "$user_info_file"; then
    mkfs.vfat -F 32 -n $efi_name ${partition[1]}
    mount --mkdir ${partition[1]} /mnt${efi_mount_point}
fi
