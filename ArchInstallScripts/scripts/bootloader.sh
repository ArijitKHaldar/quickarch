#!/usr/bin/env bash

efi_mount_point=$(sed -n 's/.*EFI_location=\(.*\).*/\1/p' "$user_info_file")
drive=$(sed -n 's/.*Drive=\(.*\).*/\1/p' "$user_info_file")

if grep -iq "grub" "$user_info_file"; then
    if grep -iq "uefi" "$user_info_file"; then
        efi_mount_point=$(sed -n 's/.*EFI_location=\(.*\).*/\1/p' "$user_info_file")
        grub-install --target=x86_64-efi --efi-directory=${efi_mount_point} --bootloader-id=grub_uefi_arch --recheck
        cp /usr/share/locale/en\@quot/LC_MESSAGES/grub.mo /boot/grub/locale/en.mo
        grub-mkconfig -o /boot/grub/grub.cfg
    else
        grub-install --target=i386-pc $drive
        cp /usr/share/locale/en\@quot/LC_MESSAGES/grub.mo /boot/grub/locale/en.mo
        grub-mkconfig -o /boot/grub/grub.cfg
    fi
elif grep -iq "systemd_boot" "$user_info_file"; then
    if grep -iq "uefi" "$user_info_file"; then
        bootctl install --esp-path=${efi_mount_point}
    fi
fi
