#!/usr/bin/env bash

hostnme=$(sed -n 's/.*Hostname=\(.*\).*/\1/p' "$user_info_file")

echo "$hostnme" >> /etc/hostname

echo "127.0.0.1    localhost" >> /etc/hosts
echo "::1    localhost" >> /etc/hosts
echo "127.0.1.1    $hostnme.localdomain    $hostnme" >> /etc/hosts
