#!/usr/bin/env bash

kernel=$(sed -n 's/.*Kernel=\(.*\).*/\1/p' "$user_info_file")

if grep -iq "btrfs" "$user_info_file"; then
    sed -i '/.*MODULES=(*/a MODULES=(btrfs)' /etc/mkinitcpio.conf
fi

mkinitcpio -p $kernel
