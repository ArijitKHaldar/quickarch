#!/usr/bin/env bash

kernel=$(sed -n 's/.*EFI_Kernel=\(.*\).*/\1/p' "$user_info_file")

pacman -S --noconfirm reflector pacman-contrib base-devel
echo -ne "Syncing Mirrors...This will take some time..."
reflector --protocol 'http,https' --delay 0.25 --score 200 --fastest 20 --sort rate --save /etc/pacman.d/mirrorlist --verbose &> /dev/null
echo -ne "Saved fastest mirrors. Proceeding to installation..."

if [[ $kernel == "linux-lts" ]]; then
    pacman -S --noconfirm linux-lts linux-lts-headers
elif [[ $kernel == "linux" ]]; then
    pacman -S --noconfirm linux linux-headers
fi

# Install the packages from official repository
pacman -S --noconfirm $(awk 'NF && $1!~/^#/' $pacman_packages)
