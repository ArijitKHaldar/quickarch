#!/usr/bin/env bash

passwd1=$(sed -n 's/.*Root_password=\(.*\).*/\1/p' "$user_info_file")
username=$(sed -n 's/.*Sudo_user=\(.*\).*/\1/p' "$user_info_file")
passwd2=$(sed -n 's/.*Sudo_password=\(.*\).*/\1/p' "$user_info_file")

if [ $(whoami) = "root"  ]; then
    echo "root:${passwd1}" | chpasswd
    groupadd libvirt
    useradd -m -G wheel,libvirt,kvm,video,audio -s $username
    echo "$username:$passwd2" | chpasswd
fi

sed -i 's/^# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers
