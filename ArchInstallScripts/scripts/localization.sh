#!/usr/bin/env bash

sed -i 's/^#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
if grep -iq "Other_locale=IN" "$user_info_file"; then
    sed -i 's/^#en_IN.UTF-8/en_IN.UTF-8/' /etc/locale.gen
    locale-gen
    echo "LC_TIME=en_IN.UTF-8" >> /etc/locale.conf
    echo "LC_MONETARY=en_IN.UTF-8" >> /etc/locale.conf
    echo "LC_MEASUREMENT=en_IN.UTF-8" >> /etc/locale.conf
    echo "LC_NUMERIC=en_IN.UTF-8" >> /etc/locale.conf
    echo "LC_PAPER=en_IN.UTF-8" >> /etc/locale.conf
fi

if grep -iq "Keymap" "$user_info_file"; then
    kmap=$(sed -n 's/.*Keymap=\(.*\).*/\1/p' "$user_info_file")
    echo "KEYMAP=$kmap" >> /etc/vconsole.conf
fi
